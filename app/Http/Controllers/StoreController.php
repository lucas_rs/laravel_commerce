<?php namespace CodeCommerce\Http\Controllers;

use CodeCommerce\Category;
use CodeCommerce\Http\Requests;
use CodeCommerce\Http\Controllers\Controller;

use CodeCommerce\Product;
use Illuminate\Http\Request;

class StoreController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        $pFeatured = Product::featured()->get();
        $pRecommend = Product::recommend()->get();

        return view('store.index', compact('categories', 'pFeatured', 'pRecommend'));
	}

    public function category($category_id)
    {
        $category = Category::find($category_id);
        $categories = Category::all();
        $products = Product::byCategory($category_id)->get();

        return view('store.category', compact('category', 'categories', 'products'));

    }

}
