<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Categorias</h2>
        <div class="panel-group category-products" id="accordian">

            @forelse($categories as $c)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="{{ route('store.category', [$c->id]) }}">{{ $c->name }}</a></h4>
                </div>
            </div>
            @empty
            @endforelse

        </div><!--/category-products-->
    </div>
</div>