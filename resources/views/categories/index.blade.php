@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Categories <a href="{{ route('categories.create') }}" class="btn btn-primary pull-right">New Category</a></h2>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->description }}</td>
                            <td>
                                <a href="{{ route('categories.edit', [$category->id]) }}">Edit</a> |
                                <a href="{{ route('categories.destroy', [$category->id]) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if(count($categories))
                    {!! $categories->render() !!}
                @endif

            </div>
        </div>
    </div>
@stop