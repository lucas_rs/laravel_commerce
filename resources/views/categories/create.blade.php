@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Category</h2>

                @include('partials.form-errors')

                {!! Form::open(['route' => 'categories.store']) !!}

                @include('categories.partials.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop