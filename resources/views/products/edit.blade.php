@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Product: {{ $product->name }}</h2>

                @include('partials.form-errors')

                {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'PUT']) !!}

                @include('products.partials.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop