<!-- category_id -->
<div class="form-group">
    {!! Form::label('category_id', 'Category') !!}
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
</div>

<!-- name -->
<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- description -->
<div class="form-group">
    {!! Form::label('description', 'Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- tags -->
<div class="form-group">
    {!! Form::label('tags', 'Tags ( separe as tags por "," )') !!}
    {!! Form::textarea('tags', (isset($product->tag_list) ? $product->tag_list : null), ['class' => 'form-control']) !!}
</div>

<!-- price -->
<div class="form-group">
    {!! Form::label('price', 'Price') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- featured -->
<div class="form-group">
    {!! Form::label('featured', 'Featured') !!}
    {!! Form::checkbox('featured', true) !!}
</div>

<!-- recommend -->
<div class="form-group">
    {!! Form::label('recommend', 'Recommend') !!}
    {!! Form::checkbox('recommend', true) !!}
</div>

<div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>