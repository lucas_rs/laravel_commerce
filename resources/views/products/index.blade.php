@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Products <a href="{{ route('products.create') }}" class="btn btn-primary pull-right">New Product</a></h2>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ str_limit($product->description, 100, '...') }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>
                                <a href="{{ route('products.edit', [$product->id]) }}">Edit</a> |
                                <a href="{{ route('products.images', [$product->id]) }}">Images</a> |
                                <a href="{{ route('products.destroy', [$product->id]) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if(count($products))
                    {!! $products->render() !!}
                @endif

            </div>
        </div>
    </div>
@stop